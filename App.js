import 'react-native-gesture-handler';
import React, {Component} from 'react';
import { View, Text, Button, TouchableOpacity,
  Image, StyleSheet, TextInput} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { useNavigation } from '@react-navigation/native';


function HomeScreen({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={styles.backgroundContainer}>
          <Image style={styles.bakcgroundImage} source={require('./img/fondo.jpg')} />
      </View>
			<View style={styles.imagen}>
			<Image source={require('./img/logo.png')} />
			</View>
			  <View style={styles.text}>
			  	<Text style={styles.text2}> Ingrese su usuario </Text>
				<TextInput
			  	style={{height: 40, color:'white',borderColor: 'gray', borderWidth: 1}}
			  />
			  </View>
			  <View style={styles.text}>
			  	<Text style={styles.text2}> Ingrese su contraseña </Text>
				<TextInput
          style={{height: 40, color:'white', borderColor: 'gray', borderWidth: 1}}
          secureTextEntry={true}
			  />
			  </View>
			  <TouchableOpacity
				style={styles.button}
				onPress={() => navigation.navigate('Details')}
			>
				<Text>Ingresar</Text>
			</TouchableOpacity>

			  		  
			</View>
  );
}



function DetailsScreen() {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Usuario logeado</Text>
    </View>
  );
}

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Details" component={DetailsScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}


const styles = StyleSheet.create({
	container:{
		flex: 1,
		justifyContent: 'center',
		paddingHorizontal: 10,
	},
	imagen:{
		flex:4,
		justifyContent:'center',
		alignItems:'center',
	},
	text:{
		padding: 25,
		justifyContent:'center',
		fontSize:30, 
    fontWeight: 'bold',
    
  },
  text2:{
		padding: 25,
		justifyContent:'center',
		fontSize:30, 
    fontWeight: 'bold',
    color:'white'
	},
	button:{
		
		alignItems: "center",
		backgroundColor: "#DDDDDD",
		padding: 25,
		

  },
  backgroundContainer: {
        flex: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
    }
})


export default App;